package controller;


import java.util.Scanner;

import model.data_structures.ArregloDinamico;
import model.logic.MVCModelo;
import model.logic.Viaje;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;

	/* Instancia de la Vista*/
	private MVCView view;

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo=new MVCModelo();
	}

	public void run() 
	{
		int tamanoInicial = 4001;
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		ArregloDinamico<Viaje> viajesHora=null;
		Integer dato1 = 0;
		Integer dato2 = 0;
		String respuesta = "";
		String respuesta1 = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:
				System.out.println("--------- \nCargando... ");
				modelo.cargarVertices();
				modelo.cargarArcos();
				System.out.println("Archivo CSV cargado");
				view.printCargar(modelo.darGrafo().V(),modelo.darGrafo().E());
				break;
			case 2:
				System.out.println("--------- \nExportando grafo");
				modelo.exportarArcos();
				modelo.exportarVertices();
				//modelo.exportarGrafo();
				System.out.println("Grafo exportado");
				break;
			case 3:
				System.out.println("--------- \nImportar Grafo");
				System.out.println("--------- \nDar ruta de archivo de vertices (ej: pruebaVertices.json)");
				respuesta1=lector.next();
				System.out.println("--------- \nDar ruta de archivo de arcos (ej: pruebaArcos.json)");
				respuesta=lector.next();
				modelo.importarVertices(respuesta1);
				modelo.importarArcos(respuesta);
				System.out.println("Grafo importado");
				view.printImportar(modelo.darGrafo().V(),modelo.darGrafo().E());
				break;
			case 4:
				System.out.println("El numero de componentes conectadas es: "+modelo.componentesConectadas());
			case 5:
				System.out.println("--------- \nExportando grafo delimitado");
				modelo.exportarVerticesDelimitados(-74.094723, -74.062707, 4.597714, 4.621360);
				modelo.exportarArcosDelimitados(-74.094723, -74.062707, 4.597714, 4.621360);
				System.out.println("Grafo delimitado exportado");
				break;
			case 6:
				System.out.println("Visualizar grafo delimitado en mapbox en el siguiente link:");
				System.out.println("https://api.mapbox.com/styles/v1/paulinaacosta/ck2zo5g3008kb1do9ktbj4d3w.html?fresh=true&title=view&access_token=pk.eyJ1IjoicGF1bGluYWFjb3N0YSIsImEiOiJjazJ3b2FkazQwaGw1M21wZ2lsaGVnZjM4In0.DLMAb6ndFjq6nihqk2wAiA#12.8/4.608630/-74.073789/0");
				break;
			default: 
				System.out.println("--------- \n Opcion Invalida !! \n---------");
				break;
			}
		}

	}	
}
