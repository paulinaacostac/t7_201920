package model.data_structures;

import java.util.Iterator;

import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;

public class SequentialSearchST<Key extends Comparable<Key>,Value>
{
	Node<Key,Value> primerNodo;
	private int tamano;

	public SequentialSearchST()
	{
		tamano=0;
	}
	public Value get(Key key)
	{ 
		Value valor=null;
		for (Node<Key,Value> x = primerNodo; x != null; x = x.darSiguiente())
		{
			if (key.equals(x.getKey()))
			{
				valor=x.getValue();
			}
		}
		return valor; 
	}
	public Iterator<Value> getSet(Key key)
	{ 
		Iterator<Value> valor=null;
		for (Node<Key,Value> x = primerNodo; x != null; x = x.darSiguiente())
		{
			if (key.equals(x.getKey()))
			{
				valor=x.getValues();
			}
		}
		return valor; 
	}
	public void put(Key key, Value val)
	{ // Search for key. Update value if found; grow table if new.
		for (Node<Key,Value> x = primerNodo; x != null; x = x.darSiguiente())
		{
			if (key.equals(x.getKey()))
			{
				// Si habia un Iterator se elimina
				x.cambiarValor(val);
				tamano++;
				return;
			} 
		}
		primerNodo = new Node<Key,Value>(key, val,primerNodo);
		tamano++;
	}
	public void putInSet(Key key, Value val)
	{ // Search for key. Add value if found; grow table if new.
		for (Node<Key,Value> x = primerNodo; x != null; x = x.darSiguiente())
		{
			if (key.equals(x.getKey()))
			{
				x.agregarValor(val);
				tamano++;
				return;
			} 
		}
		primerNodo = new Node<Key,Value>(key, val,primerNodo);
		tamano++;
	}
	
	public Value delete(Key key)
	{
		boolean eliminado = false;
		Value valorEliminado = null;
		if (primerNodo.getKey().equals(key)) 
		{
			valorEliminado = primerNodo.getValue();
			primerNodo = primerNodo.darSiguiente();
			return valorEliminado;
		}
		Node<Key, Value> actual = primerNodo;
		Node<Key, Value> siguiente = actual.darSiguiente();
		while (siguiente != null && !eliminado) {
			if (key.equals(siguiente.getKey())) 
			{
				valorEliminado = siguiente.getValue();
				System.out.println(valorEliminado.toString());
				actual.cambiarSiguiente(siguiente.darSiguiente());
				eliminado = true;
				tamano--;
			}
			actual= siguiente;
			siguiente = siguiente.darSiguiente();
		}
		return valorEliminado;
	}
	
	public Iterator<Value> deleteSet(Key key)
	{
		boolean eliminado = false;
		Iterator<Value> valoresEliminados = null;
		if (primerNodo.getKey().equals(key)) 
		{
			valoresEliminados = primerNodo.getValues();
			primerNodo = primerNodo.darSiguiente();
			return valoresEliminados;
		}
		Node<Key, Value> actual = primerNodo;
		Node<Key, Value> siguiente = actual.darSiguiente();
		while (siguiente != null && !eliminado) {
			if (key.equals(siguiente.getKey())) 
			{
				valoresEliminados = siguiente.getValues();
				actual.cambiarSiguiente(siguiente.darSiguiente());
				eliminado = true;
				tamano--;
			}
			actual= siguiente;
			siguiente = siguiente.darSiguiente();
		}
		return valoresEliminados;
	}
	public Iterator<Key> getKeys() 
	{
		Queue<Key> keys = new Queue<Key>();
		for (Node<Key,Value> x = primerNodo; x != null; x = x.darSiguiente())
		{
			keys.enqueue(x.getKey());
		}
		return keys;
	}
	public int getsize()
	{
		return tamano;
	}
}
