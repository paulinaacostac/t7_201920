/**
 * 
 */
package model.data_structures;

import java.util.Iterator;

/**
 * @author Team 1
 *
 */
public class Graph<K extends Comparable<K>, V> {

	private int V;
	private int E;
	private LinearProbingHashST<K, Vertex> adj;

	private class Vertex{

		private K key;
		private V info;
		private boolean mark;
		private ArregloDinamico<Edge> adjacents;
		private int cc;

		public Vertex(K key, V infoVertex) {
			this.mark = false;
			this.key = key;
			this.info = infoVertex;
			adjacents = new ArregloDinamico<Edge>();
			this.cc = -1;
		}

		public void mark() {
			mark = true;
		}

		public void setCC(int cc) {
			this.cc = cc;
		}
		
		public int getCC()
		{
			return cc;
		}

		public void unmark() {
			cc = -1;
			mark = false;
		}

		public boolean isMarked(){
			return mark;
		}

		public boolean equals(Vertex o) 
		{
			return o.key.equals(o.key);
		}

		public void addEdge(Edge e){
			adjacents.agregar(e);
		}

		public Iterator<K> adj()
		{
			Queue<K> iter = new Queue<K>();
			for (int i = 0; i < adjacents.darTamano(); i++) {
				iter.enqueue(adjacents.darElemento(i).other(this).key);
			}
			return iter;
		}

		public void dfs()
		{
			mark();
			for (int i = 0; i < adjacents.darTamano(); i++) 
			{
				Edge e = adjacents.darElemento(i);
				Vertex other = e.other(this);
				if (!other.isMarked()) other.dfs();
			}
		}

		public void markCC(int cc)
		{
			mark();
			setCC(cc);
			for (int i = 0; i < adjacents.darTamano(); i++) 
			{
				Edge e = adjacents.darElemento(i);
				Vertex other = e.other(this);
				if (!other.isMarked()) other.markCC(cc);
			}
		}
	}

	private class Edge implements Comparable<Edge>{

		private Vertex v;
		private Vertex w;
		private double cost;


		public Edge(Vertex v, Vertex w, double cost) {
			this.v = v;
			this.w = w;
			this.cost = cost;
		}

		public Vertex either()
		{
			return v;
		}

		public Vertex other(Vertex v) 
		{
			if (v.equals(this.v)) return w;
			else return this.v;
		}

		public int compareTo(Edge o){
			if		(this.cost > o.cost) return +1;
			else if (this.cost < o.cost) return -1;
			else return 0;
		}

	}

	public Graph() {
		adj = new LinearProbingHashST<K,Vertex>();
		E = 0;
		V = 0;
	}

	public int V() {
		return V;
	}

	public int E() {
		return E;
	}

	public void addEdge(K idVertexIni, K idVertexFin, double cost) {
		Vertex v = adj.get(idVertexIni);
		Vertex w = adj.get(idVertexFin);
		Edge e = new Edge(v, w, cost);
		v.addEdge(e);
		w.addEdge(e);
		E++;
	}

	public V getInfoVertex(K idVertex) 
	{
		if (adj.get(idVertex)!=null)
		{
			return adj.get(idVertex).info;
		}
		return null;

	}

	public void setInfoVertex(K idVertex, V infoVertex) {
		adj.get(idVertex).info = infoVertex;
	}

	public double getCostArc(K idVertexIni, K idVertexFin) {
		Vertex v = adj.get(idVertexIni);
		Vertex w = adj.get(idVertexFin);
		ArregloDinamico<Edge> edges =adj.get(idVertexIni).adjacents;
		for (int i = 0; i < edges.darTamano(); i++) 
		{
			Edge e = edges.darElemento(i);
			Vertex temp = e.either();
			if (temp.equals(v) & e.other(temp).equals(w)) return e.cost;
			else if (temp.equals(w) & e.other(temp).equals(v)) return e.cost;
		}
		return -1;
	}

	public void setCostArc(K idVertexIni, K idVertexFin, double cost) {
		Vertex v = adj.get(idVertexIni);
		Vertex w = adj.get(idVertexFin);
		ArregloDinamico<Edge> edges =adj.get(idVertexIni).adjacents;
		for (int i = 0; i < edges.darTamano(); i++) 
		{
			Edge e = edges.darElemento(i);
			Vertex temp = e.either();
			if (temp.equals(v) & e.other(temp).equals(w)) {
				e.cost = cost;
				return;
			}
			else if (temp.equals(w) & e.other(temp).equals(v)) {
				e.cost = cost;
				return;
			}
		}
	}

	public void addVertex(K idVertex, V infoVertex) {
		Vertex vertex = new Vertex(idVertex, infoVertex);
		if (!adj.contains(idVertex)) {
			V++;
		}
		adj.put(idVertex, vertex);
	}

	public Iterator<K> adj(K idVertex){
		return adj.get(idVertex).adj();
	}

	public void uncheck() {
		Iterator<K> keys = adj.keys();
		while (keys.hasNext()) 
		{
			K k = (K) keys.next();
			Vertex tempV = adj.get(k);
			tempV.unmark();
		}
	}

	public void dfs(K s) 
	{
		adj.get(s).dfs();
	}

	public int cc() {
		int numCC = 1;
		Iterator<K> keys = adj.keys();
		while (keys.hasNext()) 
		{
			K k = (K) keys.next();
			Vertex temp = adj.get(k);
			if (!temp.isMarked()) 
			{
				temp.markCC(numCC);
				numCC++;
			}
		}
		return numCC-1;
	}

	public Iterator<K> getCC(K idVertex){
		int cc = adj.get(idVertex).getCC();
		Queue<K> iter = new Queue<K>();
		Iterator<K> ver = adj.keys();
		while (ver.hasNext()) {
			K k = (K) ver.next();
			Vertex temp = adj.get(k);
			if (temp.getCC()== cc) iter.enqueue(k);
		}
		return iter;
	}
	
	public boolean isMarked(K id) {
		return adj.get(id).isMarked();
	}
	

}
