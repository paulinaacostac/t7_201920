package model.logic;

public class Feature 
{
	private String type;
	private Properties properties;
	private Geometry geometry;

	
	public Geometry getGeometry()
	{
		return geometry;
	}
	public Properties getProperties()
	{
		return properties;
	}

}
