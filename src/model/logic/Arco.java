package model.logic;

import model.data_structures.ArregloDinamico;

public class Arco implements Comparable<Arco>
{
	private ArregloDinamico<Integer> verticesAdyacentes;
	private double distancia;
	
	public Arco (ArregloDinamico<Integer> vertices, double pDistancia)
	{
		verticesAdyacentes=vertices;
		distancia=pDistancia;
	}
	public ArregloDinamico<Integer> darVerticesAdyacentes()
	{
		return verticesAdyacentes;
	}
	public double darDistancia()
	{
		return distancia;
	}
	public String toString()
	{
		String r="";
		for (int i=0;i<verticesAdyacentes.darTamano();i++)
		{
			r+="Vertice "+i+" : "+verticesAdyacentes.darElemento(i)+"\n";
		}
		r+="Distancia: "+distancia;
		return r;
	}
	@Override
	public int compareTo(Arco o) {
		// TODO Auto-generated method stub
		return 0;
	}

}
