package model.logic;

import com.google.gson.annotations.SerializedName;

public class Properties 
{
	private double distancia;
	private int idv1;
	private int idv2;
	
	public double darDistancia() 
	{
		return distancia;
	}
	
	public int daridV1() 
	{
		return idv1;
	}
	
	public int daridV2() 
	{
		return idv2;
	}

}
