package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.Iterator;

import com.opencsv.CSVReader;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

import model.data_structures.ArregloDinamico;
import model.data_structures.Esquina;
import model.data_structures.Graph;
import model.data_structures.RedBlackBST;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo
{
	/**
	 * Atributos del modelo del mundo
	 */
	private int numeroArcos;
	private Graph<Integer, Vertice> grafo;
	private ArregloDinamico<Arco> arcos;
	private ArregloDinamico<Vertice> vertices;

	public MVCModelo()
	{

		arcos=new ArregloDinamico<Arco>(1000);
		vertices=new ArregloDinamico<Vertice>(1000);
		grafo=new Graph<Integer,Vertice>();
		numeroArcos=0;

	}
	public void exportarVerticesDelimitados(double longMin, double longMax, double LatMin, double LatMax)
	{
		PrintWriter writer;
		int c=0;
		ArregloDinamico<Vertice> verticesEnZona=new ArregloDinamico<Vertice>(7000);
		try 
		{
			writer = new PrintWriter("pruebaVerticesDelimitados.json", "UTF-8");
			writer.println("{\"type\":\"FeatureCollection\",\"features\":[");
			for (int i=0;i<grafo.V();i++)
			{
				Vertice v=vertices.darElemento(i);
				if (v.darLatitud()>=LatMin && v.darLatitud()<=LatMax && v.darLongitud()>=longMin && v.darLongitud()<=longMax)
				{
					verticesEnZona.agregar(v);
				}
			}
			for (int i=0;i<verticesEnZona.darTamano();i++)
			{
				c++;
				writer.println("{");
				writer.println("\"type\":\"Feature\",");
				writer.println("\"properties\":{\"id\":"+verticesEnZona.darElemento(i).darId()+", \"zona\":"+verticesEnZona.darElemento(i).darZona()+"},");
				writer.println("\"geometry\":{\"coordinates\": ["+verticesEnZona.darElemento(i).darLongitud()+","+verticesEnZona.darElemento(i).darLatitud()+"],"+"\"type\":\"Point\"}");
				if (i!=verticesEnZona.darTamano()-1)
				{
					writer.println("},");
				}
				else
					writer.println("}");
			}
			writer.println("]}");
			writer.close();
			System.out.println("Vertices exportados en grafo delimitado: "+c);
		} 
		catch (FileNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (UnsupportedEncodingException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void exportarArcosDelimitados(double longMin, double longMax, double LatMin, double LatMax)
	{
		PrintWriter writer;
		ArregloDinamico<Arco> arcosEnZona=new ArregloDinamico<Arco>(7000);
		int c=0;
		try 
		{
			writer = new PrintWriter("pruebaArcosDelimitados.json", "UTF-8");
			writer.println("{\"type\":\"FeatureCollection\",\"features\":[");
			for (int i=0;i<grafo.E();i++)
			{
				int idV1=arcos.darElemento(i).darVerticesAdyacentes().darElemento(0);
				int idV2=arcos.darElemento(i).darVerticesAdyacentes().darElemento(1);
				Vertice v1=grafo.getInfoVertex(idV1);
				Vertice v2=grafo.getInfoVertex(idV2);
				if (v1!=null &&v2!=null&&v1.darLatitud()>=LatMin && v1.darLatitud()<=LatMax && v1.darLongitud()>=longMin && v1.darLongitud()<=longMax && v2.darLatitud()>=LatMin && v2.darLatitud()<=LatMax && v2.darLongitud()>=longMin && v2.darLongitud()<=longMax)
				{
					arcosEnZona.agregar(arcos.darElemento(i));
				}
			}
			for (int i=0;i<arcosEnZona.darTamano();i++)
			{
				int idV1=arcosEnZona.darElemento(i).darVerticesAdyacentes().darElemento(0);
				int idV2=arcosEnZona.darElemento(i).darVerticesAdyacentes().darElemento(1);
				Vertice v1=grafo.getInfoVertex(idV1);
				Vertice v2=grafo.getInfoVertex(idV2);
				c++;
				writer.println("{");
				writer.println("\"type\":\"Feature\",");
				writer.println("\"properties\":{\"distancia\":"+arcos.darElemento(i).darDistancia()+", \"idv1\":"+idV1+", \"idv2\":"+idV2+"},");
				writer.println("\"geometry\":{\"coordinates\": [["+v1.darLongitud()+","+v1.darLatitud()+"],["+v2.darLongitud()+","+v2.darLatitud()+"]],\"type\":\"LineString\"}");
				if (i!=arcosEnZona.darTamano()-1)
				{
					writer.println("},");
				}
				else
					writer.println("}");
			}
			writer.println("]}");
			writer.close();
			System.out.println("Arcos exportados en grafo delimitado: "+c);
		} 
		catch (FileNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (UnsupportedEncodingException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void exportarGrafo()
	{
		PrintWriter writer;
		try 
		{
			writer = new PrintWriter("pruebaGrafo.json", "UTF-8");
			writer.println("{\"type\":\"FeatureCollection\",\"features\":[");
			for (int i=0;i<grafo.E();i++)
			{
				int idV1=arcos.darElemento(i).darVerticesAdyacentes().darElemento(0);
				int idV2=arcos.darElemento(i).darVerticesAdyacentes().darElemento(1);
				Vertice v1=grafo.getInfoVertex(idV1);
				Vertice v2=grafo.getInfoVertex(idV2);
				writer.println("{");
				writer.println("\"type\":\"Feature\",");
				writer.println("\"properties\":{\"distancia\":"+arcos.darElemento(i).darDistancia()+", \"idv1\":"+idV1+", \"idv2\":"+idV2+"},");
				writer.println("\"geometry\":{\"coordinates\": [["+v1.darLongitud()+","+v1.darLatitud()+"],["+v2.darLongitud()+","+v2.darLatitud()+"]],\"type\":\"LineString\"}");
				if (i!=grafo.E()-1)
				{
					writer.println("},");
				}
				else
					writer.println("}");
			}
			writer.println("]}");
			writer.println("{\"type\":\"FeatureCollection\",\"features\":[");
			for (int i=0;i<grafo.V();i++)
			{
				writer.println("{");
				writer.println("\"type\":\"Feature\",");
				writer.println("\"properties\":{\"id\":"+vertices.darElemento(i).darId()+", \"zona\":"+vertices.darElemento(i).darZona()+"},");
				writer.println("\"geometry\":{\"coordinates\": ["+vertices.darElemento(i).darLongitud()+","+vertices.darElemento(i).darLatitud()+"],"+"\"type\":\"Point\"}");
				if (i!=grafo.V()-1)
				{
					writer.println("},");
				}
				else
					writer.println("}");
			}
			writer.println("]}");
			writer.close();
		} 
		catch (FileNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (UnsupportedEncodingException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void exportarArcos()
	{
		PrintWriter writer;
		try 
		{
			writer = new PrintWriter("pruebaArcos.json", "UTF-8");
			writer.println("{\"type\":\"FeatureCollection\",\"features\":[");
			for (int i=0;i<grafo.E();i++)
			{
				int idV1=arcos.darElemento(i).darVerticesAdyacentes().darElemento(0);
				int idV2=arcos.darElemento(i).darVerticesAdyacentes().darElemento(1);
				Vertice v1=grafo.getInfoVertex(idV1);
				Vertice v2=grafo.getInfoVertex(idV2);
				writer.println("{");
				writer.println("\"type\":\"Feature\",");
				writer.println("\"properties\":{\"distancia\":"+arcos.darElemento(i).darDistancia()+", \"idv1\":"+idV1+", \"idv2\":"+idV2+"},");
				writer.println("\"geometry\":{\"coordinates\": [["+v1.darLongitud()+","+v1.darLatitud()+"],["+v2.darLongitud()+","+v2.darLatitud()+"]],\"type\":\"LineString\"}");
				if (i!=grafo.E()-1)
				{
					writer.println("},");
				}
				else
					writer.println("}");
			}
			writer.println("]}");
			writer.close();
		} 
		catch (FileNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (UnsupportedEncodingException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void exportarVertices()
	{
		PrintWriter writer;
		try 
		{
			writer = new PrintWriter("pruebaVertices.json", "UTF-8");
			writer.println("{\"type\":\"FeatureCollection\",\"features\":[");
			for (int i=0;i<grafo.V();i++)
			{
				writer.println("{");
				writer.println("\"type\":\"Feature\",");
				writer.println("\"properties\":{\"id\":"+vertices.darElemento(i).darId()+", \"zona\":"+vertices.darElemento(i).darZona()+"},");
				writer.println("\"geometry\":{\"coordinates\": [["+vertices.darElemento(i).darLongitud()+","+vertices.darElemento(i).darLatitud()+"]],"+"\"type\":\"Point\"}");
				if (i!=grafo.V()-1)
				{
					writer.println("},");
				}
				else
					writer.println("}");
			}
			writer.println("]}");
			writer.close();
		} 
		catch (FileNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (UnsupportedEncodingException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}
	public void importarArcos(String ruta)
	{
		Gson gson = new Gson();
		try (Reader reader = new FileReader(ruta)) 
		{
			ArregloJSON az=gson.fromJson(reader, ArregloJSON.class);
			//			System.out.println(az.darFeatures()[0].getProperties().toString());
			numeroArcos=az.darFeatures().length;
			for (int i=0;i<az.darFeatures().length;i++)
			{
				int v1=az.darFeatures()[i].getProperties().daridV1();
				int v2=az.darFeatures()[i].getProperties().daridV2();
				ArregloDinamico<Integer> verticesAdyacentes=new ArregloDinamico<Integer>(2);
				verticesAdyacentes.agregar(v1);
				verticesAdyacentes.agregar(v2);
				double distancia=az.darFeatures()[i].getProperties().darDistancia();
				//				Vertice ver1=new Vertice(v1, 4565, 456, 4564);
				//				Vertice ver2=new Vertice(v2, 4565, 456, 4564);
				//				grafo.addVertex(v1, ver1);
				//				grafo.addVertex(v2, ver2);
				arcos.agregar(new Arco(verticesAdyacentes,distancia));
				grafo.addEdge(v1, v2, distancia);
			}
			//			System.out.println(zonas.darElemento(1).darCoordenadas().darElemento(0).darLatitud()+" Latitud");
			//			System.out.println(zonas.darElemento(1).darCoordenadas().darElemento(0).darLongitud()+" Longitud");
			System.out.println("Num arcos: "+arcos.darTamano());
			System.out.println(arcos.darElemento(0).toString());
			System.out.println(arcos.darElemento(1).toString());
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

	public void importarVertices(String ruta)
	{
		Gson gson = new Gson();
		try (Reader reader = new FileReader(ruta)) 
		{
			ArregloVertices az=gson.fromJson(reader, ArregloVertices.class);
			//			System.out.println(az.darFeatures()[0].getProperties().toString());
			numeroArcos=az.darFeatures().length;
			for (int i=0;i<az.darFeatures().length;i++)
			{
				int id=az.darFeatures()[i].getProperties().darId();
				int zona=az.darFeatures()[i].getProperties().darZona();
				double latitud=az.darFeatures()[i].getGeometry().getCoordinates()[0][0];
				double longitud=az.darFeatures()[i].getGeometry().getCoordinates()[0][1];
				//				Vertice ver1=new Vertice(v1, 4565, 456, 4564);
				//				Vertice ver2=new Vertice(v2, 4565, 456, 4564);
				//				grafo.addVertex(v1, ver1);
				//				grafo.addVertex(v2, ver2);
				Vertice v=new Vertice(id,longitud,latitud,zona);
				vertices.agregar(v);
				grafo.addVertex(id, v);
			}
			//			System.out.println(zonas.darElemento(1).darCoordenadas().darElemento(0).darLatitud()+" Latitud");
			//			System.out.println(zonas.darElemento(1).darCoordenadas().darElemento(0).darLongitud()+" Longitud");

		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}


	//	public void importarGrafo()
	//	{
	//		Gson gson = new Gson();
	//		try (Reader reader = new FileReader("pruebaGrafo.json")) 
	//		{
	//			ArregloVertices az=gson.fromJson(reader, ArregloVertices.class);
	////			System.out.println(az.darFeatures()[0].getProperties().toString());
	//			numeroArcos=az.darFeatures().length;
	//			for (int i=0;i<az.darFeatures().length;i++)
	//			{
	//				int v1=az.darFeatures()[i].getProperties().daridV1();
	//				int v2=az.darFeatures()[i].getProperties().daridV2();
	//				ArregloDinamico<Integer> verticesAdyacentes=new ArregloDinamico<Integer>(2);
	//				verticesAdyacentes.agregar(v1);
	//				verticesAdyacentes.agregar(v2);
	//				double distancia=az.darFeatures()[i].getProperties().darDistancia();
	////				Vertice ver1=new Vertice(v1, 4565, 456, 4564);
	////				Vertice ver2=new Vertice(v2, 4565, 456, 4564);
	////				grafo.addVertex(v1, ver1);
	////				grafo.addVertex(v2, ver2);
	//				arcos.agregar(new Arco(verticesAdyacentes,distancia));
	//				grafo.addEdge(v1, v2, distancia);
	//			}
	////			System.out.println(zonas.darElemento(1).darCoordenadas().darElemento(0).darLatitud()+" Latitud");
	////			System.out.println(zonas.darElemento(1).darCoordenadas().darElemento(0).darLongitud()+" Longitud");
	//			System.out.println("Num arcos: "+arcos.darTamano());
	//			System.out.println(arcos.darElemento(0).toString());
	//			System.out.println(arcos.darElemento(1).toString());
	//		} 
	//		catch (IOException e) 
	//		{
	//			e.printStackTrace();
	//		}
	//		
	//		
	//		Gson gson = new Gson();
	//		try (Reader reader = new FileReader("pruebaArcos.json")) 
	//		{
	//			ArregloJSON az=gson.fromJson(reader, ArregloJSON.class);
	////			System.out.println(az.darFeatures()[0].getProperties().toString());
	//			numeroArcos=az.darFeatures().length;
	//			for (int i=0;i<az.darFeatures().length;i++)
	//			{
	//				int v1=az.darFeatures()[i].getProperties().daridV1();
	//				int v2=az.darFeatures()[i].getProperties().daridV2();
	//				ArregloDinamico<Integer> verticesAdyacentes=new ArregloDinamico<Integer>(2);
	//				verticesAdyacentes.agregar(v1);
	//				verticesAdyacentes.agregar(v2);
	//				double distancia=az.darFeatures()[i].getProperties().darDistancia();
	////				Vertice ver1=new Vertice(v1, 4565, 456, 4564);
	////				Vertice ver2=new Vertice(v2, 4565, 456, 4564);
	////				grafo.addVertex(v1, ver1);
	////				grafo.addVertex(v2, ver2);
	//				arcos.agregar(new Arco(verticesAdyacentes,distancia));
	//				grafo.addEdge(v1, v2, distancia);
	//			}
	////			System.out.println(zonas.darElemento(1).darCoordenadas().darElemento(0).darLatitud()+" Latitud");
	////			System.out.println(zonas.darElemento(1).darCoordenadas().darElemento(0).darLongitud()+" Longitud");
	//			System.out.println("Num arcos: "+arcos.darTamano());
	//			System.out.println(arcos.darElemento(0).toString());
	//			System.out.println(arcos.darElemento(1).toString());
	//		} 
	//		catch (IOException e) 
	//		{
	//			e.printStackTrace();
	//		}
	//		
	//	}
	public void cargarVertices()
	{
		File file = new File("./data/bogota_vertices.txt");
		BufferedReader br = null;
		String nextLine="";
		try 
		{
			br = new BufferedReader(new FileReader(file));
			nextLine=br.readLine(); //saltar linea de nombre de columnas
			while ((nextLine = br.readLine()) != null)
			{
				String partes[]=nextLine.split(";");
				Vertice vertice = new Vertice(Integer.parseInt(partes[0]), Double.parseDouble(partes[1]), Double.parseDouble(partes[2]), Integer.parseInt(partes[3])); 
				grafo.addVertex(Integer.parseInt(partes[0]), vertice);
				vertices.agregar(vertice);
				//System.out.println(vertice.toString());//esquinas.agregar(esquina);
			}
		}
		catch (NumberFormatException | IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void cargarArcos()
	{
		File file = new File("./data/bogota_arcos.txt");
		BufferedReader br = null;
		String nextLine="";
		String prueba="";
		try 
		{
			br = new BufferedReader(new FileReader(file));
			while ((nextLine = br.readLine()) != null)
			{
				String partes[]=nextLine.split(" ");
				for (int i=0;i<partes.length-1;i++)
				{
					ArregloDinamico<Integer> verticesAdyacentes=new ArregloDinamico<Integer>(2);
					Vertice v=grafo.getInfoVertex(Integer.parseInt(partes[0]));
					Vertice v1=grafo.getInfoVertex(Integer.parseInt(partes[i+1]));
					if (v!=null && v1!=null)
					{
						double long1=v.darLongitud();
						double lat1=v.darLatitud();
						double long2=v1.darLongitud();
						double lat2=v1.darLatitud();
						Haversine h=new Haversine();
						double d=h.distance(lat1, long1, lat2, long2);
						verticesAdyacentes.agregar(Integer.parseInt(partes[i]));
						verticesAdyacentes.agregar(Integer.parseInt(partes[i+1]));
						Arco arco = new Arco(verticesAdyacentes, d); 
						arcos.agregar(arco);
						grafo.addEdge(v.darId(), v1.darId(), d);
					}
				}
			}
			//			System.out.println(arcos.darElemento(17).toString());
			//			System.out.println(arcos.darElemento(18).toString());
			//			System.out.println(arcos.darElemento(19).toString());
			//			System.out.println(arcos.darElemento(20).toString());
			//			System.out.println("antepenultimo "+arcos.darElemento(tamano-3).toString());
			//			System.out.println("penultimo "+arcos.darElemento(tamano-2).toString());
			//			System.out.println("ultimo "+arcos.darElemento(tamano-1).toString());
		}
		catch (NumberFormatException | IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public int componentesConectadas()
	{
		return grafo.cc();
	}
	public Graph darGrafo()
	{
		return grafo;
	}
}
