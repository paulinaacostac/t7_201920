package model.logic;

public class Viaje implements Comparable<Viaje>
{
	private int sourceid;
	private int destid;
	private int dow;
	private double meantraveltime;
	private double standarddeviationtraveltime;
	private double geometricmeantraveltime;
	private double geometricstandarddeviationtraveltime;
	
	public Viaje(int psourceid, int pdstid, int pDow, double pmeantraveltime, double pstandarddeviationtraveltime, double pgeometricmeantraveltime, double pgeometricstandarddeviationtraveltime)
	{
		sourceid=psourceid;
		destid=pdstid;
		dow=pDow;
		meantraveltime=pmeantraveltime;
		standarddeviationtraveltime=pstandarddeviationtraveltime;
		geometricmeantraveltime=pgeometricmeantraveltime;
		geometricstandarddeviationtraveltime=pgeometricstandarddeviationtraveltime;
		
	}
	public int darDow()
	{
		return dow;
	}
	public int darSourceid()
	{
		return sourceid;
	}
	public int darDstid()
	{
		return destid;
	}
	public double darMeanTravelTime()
	{
		return meantraveltime;
	}
	public double darStandardDeviationTravelTime()
	{
		return standarddeviationtraveltime;
	}
	public double darGeometricMeanTravelTime()
	{
		return geometricmeantraveltime;
	}
	public double darGeometricStandardDeviationTravelTime()
	{
		return geometricstandarddeviationtraveltime;
	}
	
	public int compareTo(Viaje pViaje)
	{
		if (this.meantraveltime<pViaje.meantraveltime)
		{
			return -1;
		}
		if (this.meantraveltime>pViaje.meantraveltime)
		{
			return 1;
		}
		if (this.meantraveltime==pViaje.meantraveltime)
		{
			return 0;
		}
		return 0;
	}
	
}
