package test.logic;
import model.data_structures.*;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;


public class TestGraph 
{
	private Graph<Integer, String> graph;
	
	@Before
	public void setUp() 
	{
		graph = new Graph<Integer, String>();
		graph.addVertex(1, "A");
		graph.addVertex(2, "B");
		graph.addVertex(3, "C");
		graph.addVertex(4, "D");
		graph.addVertex(5, "E");
		graph.addVertex(6, "F");
		graph.addVertex(7, "G");
		graph.addVertex(8, "H");
		graph.addVertex(9, "I");
		graph.addVertex(10, "J");
	}
	
	/**
	 * Grafo sin conexiones
	 */
	public void setUp1() 
	{
		return;
	}
	
	/**
	 * Grafo completamente conectado
	 */
	public void setUp2() 
	{
		for (int i = 1; i <= 10; i++) {	
			for (int j = i+1; j <= 10; j++) {
				graph.addEdge(i, j, 0);
			}
		}
	}
	
	/**
	 * Grafo con un camino camino de A-B-C
	 */
	public void setUp3() 
	{
		graph.addEdge(1, 2, 0);
		graph.addEdge(2, 3, 0);
	}
	
	/**
	 * Grafo A conectado con todos los demas
	 */
	public void setUp4() 
	{
		for (int i = 2; i <= 10; i++) {	
			graph.addEdge(1, i, 0);
		}
	}
	
	@Test
	public void testInfo()
	{
		assertTrue(graph!=null);
		assertEquals("A",graph.getInfoVertex(1));
		assertEquals("B",graph.getInfoVertex(2));
		assertEquals("C",graph.getInfoVertex(3));
		assertEquals("D",graph.getInfoVertex(4));
		assertEquals("E",graph.getInfoVertex(5));
		assertEquals("F",graph.getInfoVertex(6));
		assertEquals("G",graph.getInfoVertex(7));
		assertEquals("H",graph.getInfoVertex(8));
		assertEquals("I",graph.getInfoVertex(9));
		assertEquals("J",graph.getInfoVertex(10));
	}
	
	@Test
	public void testSetInfo()
	{
		for (int i = 1; i <=10; i++) {
			graph.setInfoVertex(i, "Z");
		}
		
		for (int i = 1; i <=10; i++) {
			assertEquals("Z",graph.getInfoVertex(i));
		}
	}
	
	@Test
	public void testUncheck()
	{
		setUp2();
		graph.dfs(1);
		for (int i = 1; i <=10; i++) {
			assertTrue(graph.isMarked(i));
		}
		graph.uncheck();
		for (int i = 1; i <=10; i++) {
			assertTrue(!graph.isMarked(i));
		}
	}
	
	@Test
	public void testdfs1()
	{
		setUp1();
		graph.dfs(1);
		assertTrue(graph.isMarked(1));
		for (int i = 2; i <=10; i++) {
			assertTrue(!graph.isMarked(i));
		}

	}
	
	@Test
	public void testdfs2()
	{
		setUp2();
		graph.dfs(1);
		for (int i = 1; i <=10; i++) {
			assertTrue(graph.isMarked(i));
		}
	}
	
	@Test
	public void testdfs3()
	{
		setUp3();
		graph.dfs(1);
		assertTrue(graph.isMarked(1));
		assertTrue(graph.isMarked(2));
		assertTrue(graph.isMarked(3));
		for (int i = 4; i <=10; i++) {
			assertTrue(!graph.isMarked(i));
		}

	}
	
	@Test
	public void testdfs4()
	{
		setUp2();
		graph.dfs(1);
		for (int i = 1; i <=10; i++) {
			assertTrue(graph.isMarked(i));
		}
	}
	
	@Test
	public void testCC1()
	{
		setUp1();
		assertEquals(10,graph.cc());
	}
	
	@Test
	public void testCC2()
	{
		setUp2();
		assertEquals(1,graph.cc());
	}
	
	@Test
	public void testCC3()
	{
		setUp3();
		assertEquals(8,graph.cc());
	}
	
	@Test
	public void testGetCC()
	{
		setUp3();
		graph.cc();
		Iterator<Integer> iter =graph.getCC(1);
		int contador = 0;
		while (iter.hasNext()) {
			Integer i = (Integer) iter.next();
			assertTrue(i==1 || i==2 || i==3);
			contador++;
		}
		assertEquals(3, contador);
	}
	
	
	
	
	
}
